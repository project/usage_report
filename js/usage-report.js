document.addEventListener('DOMContentLoaded', function () {
  // Add filters above the table with id 'usage-table' based on the data in the table defined in UsageController.php.
  var table = document.getElementById('usage-report-table');
  if (table) {
    // Get each header column from the table and create a select filter with the same name
    // as the column.
    var headers = table.getElementsByTagName('th');
    var rows = table.getElementsByTagName('tr');

    for (var i = 0; i < headers.length; i++) {
      var header = headers[i];
      var filter = document.createElement('select');
      filter.className = 'usage-report-filter';
      filter.label = i;
      // continue of filter.label is title or edit
      if (header.innerHTML == 'Title' || header.innerHTML == 'Edit') {
        continue;
      }
      // Define an empty array of options
      var options = [];
      for (var j = 0; j < rows.length; j++) {
        var row = rows[j];
        var cell = row.getElementsByTagName('td')[i];
        if (cell) {
          // If cell.innerHTML is not already in the options array, add it.
          if (options.indexOf(cell.innerHTML) == -1) {
            options.push(cell.innerHTML);
          }
        }
      }

      // If there is more than one option, add the filter to the header.
      if (options.length > 1) {
        // Add an 'all' option to the filter.
        options.unshift('All');
        // Loop through the options and create an option element for each.
        for (var k = 0; k < options.length; k++) {
          options[k] = '<option value="' + options[k] + '">' + options[k] + '</option>';
        }
        filter.innerHTML = options.join('');
        // Add an event listener to the filter to filter the table.
        filter.addEventListener('change', function (e) {


          var filter = e.target;
          var column = filter.label;
          var value = filter.value;

          for (var l = 0; l < rows.length; l++) {
            var row = rows[l];
            var cell = row.getElementsByTagName('td')[column];
            if (cell) {
              if (cell.innerText == value || value == 'All') {
                // Only show the row if it was previously hidden by this same filter and no others.
                if (row.getAttribute('data-hidden-column-' + column) == 'true') {
                  row.removeAttribute('data-hidden-column-' + column);
                }
                // If there are no attributes beginning with 'data-hidden-column-' then show the row.
                // Loop through all the attributes on the row
                var hidden = false;
                for (var m = 0; m < row.attributes.length; m++) {
                  var attribute = row.attributes[m];
                  // If the attribute name begins with 'data-hidden-column-' then the row is hidden.
                  if (attribute.name.indexOf('data-hidden-column-') == 0) {
                    hidden = true;
                  }
                }
                if (!hidden) {
                  row.style.display = '';
                }
              }
              else {
                row.style.display = 'none';
                // add a data attribute to the row
                row.setAttribute('data-hidden-column-' + column, true);
              }
            }
          }
        });
        header.appendChild(filter);
      }
    }
  }

}, false);

