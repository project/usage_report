<?php

namespace Drupal\usage_report\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Returns responses for Usage Report module routes..
 */
class UsageReportController extends ControllerBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */

  protected $moduleHandler;

  /**
   * The number of rows per page.
   *
   * @var int
   */
  protected $rowsPerPage = 100;

  /**
   * Constructs a \Drupal\usage_report\Controller\UsageReportController object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   *
   * @return void
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, CacheBackendInterface $cache, PagerManagerInterface $pager_manager, ModuleHandlerInterface $module_handler) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->cache = $cache;
    $this->pagerManager = $pager_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('cache.default'),
      $container->get('pager.manager'),
      $container->get('module_handler'),
    );
  }

  /**
   * Presents a Usage tab showing a table of entities referencing the current entity.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   * @param string $entity_type_id
   *   The id of the current entity.
   *
   * @return array
   *   Render array of page contents.
   */
  public function usage(Request $request, RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity = $route_match->getParameter($entity_type_id);
    $usage_data = $this->getUsageData($entity);

    $build['#title'] = $this->t('Usage Report for %label', args: ['%label' => $entity->label() ?? $entity->id()]);

    // If there are no references, return a message that this node
    // currently has no references.
    if (empty($usage_data)) {
      $build['empty']['#markup'] = $this->t('This entity currently has no references.');
      return $build;
    }
    if (count($usage_data) > $this->rowsPerPage) {
      $build['message'] = [
        '#markup' => $this->t('This entity is referenced in more than @count places. Only the first @count are shown, filters are ommitted, and a pager is included. The CSV Export will also only return @count items per page.', ['@count' => $this->rowsPerPage]),
      ];
    }
    $page = $request->query->get('page') ?? 0;

    $build['csv_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Download CSV'),
      '#url' => $entity->toUrl('usage-report-csv', ['query' => ['page' => $page]]),
      '#attributes' => ['class' => ['button']],
      '#prefix' => '<div class="usage-report-csv-link">',
      '#suffix' => '</div>',
    ];

    // Output all the references in a table.
    $build['table'] = [
      '#theme' => 'table',
      '#header' => [$this->t('Entity'), $this->t('Bundle'),
        $this->t('Field'), $this->t('ID'), $this->t('Title'),
        $this->t('Language'), $this->t('Published'), $this->t('Edit'),
      ],
      '#rows' => $this->getUsageRows($usage_data, $page),
      '#attributes' => ['id' => 'usage-report-table'],
    ];
    if (count($usage_data) > $this->rowsPerPage) {
      $this->pagerManager->createPager(count($usage_data), $this->rowsPerPage);

      $build['pager'] = [
        '#type' => 'pager',
      ];
    }
    // Attach a simple JS filtering if there are not more than rowsPerPage rows.
    else {
      $build['#attached']['library'][] = 'usage_report/usage-report';
    }
    return $build;
  }

  /**
   * Get the full set of usage data for an entity.
   */
  private function getUsageData(ContentEntityInterface $entity) {
    // Get usage data from cache.
    $cache = $this->cache->get("usage_report_{$entity->getEntityTypeId()}_{$entity->id()}");
    if ($cache) {
      return $cache->data;
    }
    $field_list = $this->getFieldList($entity->getEntityTypeId());
    $usage_entity_data = [];
    $cache_tags = [];
    foreach ($field_list as $entity_type => $fields) {
      $cache_tags[] = $entity_type . '_list';
      foreach ($fields as $field_name) {

        // Find any references to the current node using entity query.
        $query = $this->entityTypeManager->getStorage($entity_type)->getQuery();
        $query->condition("{$field_name}.target_id", $entity->id());

        $usage_entity_ids = $query->execute();
        foreach ($usage_entity_ids as $usage_entity_id) {
          $usage_entity_data[] = [
            'entity_type' => $entity_type,
            'field_name' => $field_name,
            'usage_entity_id' => $usage_entity_id,
          ];
        }
      }
    }
    // Sort usage_entity_data by entity_type, field_name, usage_entity_id.
    usort($usage_entity_data, function ($a, $b) {
      if ($a['entity_type'] == $b['entity_type']) {
        if ($a['field_name'] == $b['field_name']) {
          return $a['usage_entity_id'] <=> $b['usage_entity_id'];
        }
        return $a['field_name'] <=> $b['field_name'];
      }
      return $a['entity_type'] <=> $b['entity_type'];
    });

    if ($usage_entity_data) {
      $this->cache->set("usage_report_{$entity->getEntityTypeId()}_{$entity->id()}", $usage_entity_data, Cache::PERMANENT, $cache_tags);
    }
    return $usage_entity_data;
  }

  /**
   * {@inheritdoc}
   */
  private function getFieldList($target_entity_type) {
    // Get field list from cache.
    $cache = $this->cache()->get("usage_report_field_list_{$target_entity_type}");
    if ($cache) {
      return $cache->data;
    }
    else {
      $field_list = [];

      // Find all fields of type entity reference.
      $entity_reference_map = $this->entityFieldManager->getFieldMapByFieldType('entity_reference');

      // Find all fields of type entity_reference_revisions.
      $entity_reference_revisions_map = $this->entityFieldManager->getFieldMapByFieldType('entity_reference_revisions');

      // Merge the two arrays.
      $entity_reference_map = array_merge_recursive($entity_reference_map, $entity_reference_revisions_map);

      // Loop through all the reference_names field names, and query the
      // database for references with target of the current node.
      foreach ($entity_reference_map as $entity_type => $info) {
        foreach ($info as $field_name => $data) {
          if (!preg_match('/^field_.*$/', $field_name)) {
            continue;
          }
          if ($entity_type == 'field_collection_item' && $field_name == 'field_name') {
            continue;
          }

          // Find which types of entities $field_name can reference.
          $field_storage = $this->entityTypeManager->getStorage('field_storage_config')
            ->load("{$entity_type}.{$field_name}");
          $target_type = $field_storage->getSetting('target_type');
          // Make sure the field references the current entity type.
          if ($target_entity_type == $target_type) {
            $field_list[$entity_type][] = $field_name;
          }
        }
      }
      // Cache the field list.
      $this->cache->set('usage_report_field_list_' . $target_entity_type, $field_list, Cache::PERMANENT, [
        'entity_types',
        'entity_field_info',
      ]);
      return $field_list;
    }
  }

  /**
   * Prepare data for rendering in a table, for a given page.
   */
  private function getUsageRows($usage_entity_data, $page) {

    // Set the offset.
    $offset = $page * $this->rowsPerPage;
    // Get n rows from $usage_entity_data.
    $usage_entity_data_sliced = array_slice($usage_entity_data, $offset, $this->rowsPerPage);

    // Organize usage_entity_data into a new array keyed by
    // entity_type and field_name.
    $usage_entity_data_by_entity_type_and_field_name = [];
    foreach ($usage_entity_data_sliced as $usage_entity_datum) {
      $entity_type = $usage_entity_datum['entity_type'];
      $field_name = $usage_entity_datum['field_name'];
      $usage_entity_data_by_entity_type_and_field_name[$entity_type][$field_name][] = $usage_entity_datum['usage_entity_id'];
    }

    $rows = [];

    // Loop through usage_entity_data_by_entity_type_and_field_name.
    foreach ($usage_entity_data_by_entity_type_and_field_name as $entity_type => $usage_entity_data_by_field_name) {
      // Loop through usage_entity_data_by_field_name.
      foreach ($usage_entity_data_by_field_name as $field_name => $usage_entity_ids) {
        // Load all entities of type $entity_type with ids in $usage_entity_ids.
        $usage_entities = $this->entityTypeManager->getStorage($entity_type)->loadMultiple($usage_entity_ids);
        // Loop through the entities.
        foreach ($usage_entities as $usage_entity) {
          // Get the label of the field.
          $field_label = $usage_entity->get($field_name)->getFieldDefinition()->getConfig($usage_entity->bundle())->getLabel();

          // Recursively look up parents from paragraphs and field collections.
          $this->modifyEntity($usage_entity, $field_label);
          if (!$usage_entity) {
            continue;
          }
          // Load all translations of the entity.
          $languages = $usage_entity->getTranslationLanguages();
          foreach ($languages as $language_id => $language) {
            $translation = $usage_entity->getTranslation($language_id);
            // Use a key to prevent duplicate rows.
            $key = implode(':', [$usage_entity->getEntityTypeId(),
              $usage_entity->id(), $language_id,
            ]);
            $rows[$key] = [
              'entity_type' => $usage_entity->getEntityTypeId(),
              'bundle' => $usage_entity->type->entity->label(),
              'field_name' => $field_label,
              'entity_id' => $usage_entity->id(),
              'entity_link' => $translation->access('view') ? Link::fromTextAndUrl($translation->label(), $translation->toUrl())
                ->toString() : '',
              'entity_language' => $language_id,
              'entity_status' => $translation->isPublished() ? $this->t('Published') : $this->t('Unpublished'),
              'edit_link' => $translation->access('update') ? Link::fromTextAndUrl('edit', $translation->toUrl('edit-form'))
                ->toString() : '',
            ];
          }
        }
      }
    }
    return $rows;
  }

  /**
   * Modify records that are Paragraphs or Field Collection entities.
   */
  private function modifyEntity(&$usage_entity, &$field_label) {
    // If the entity type is paragraph, we need to find the parent entity
    // check if paragraphs module is enabled.
    if ($this->moduleHandler->moduleExists('paragraphs') && $usage_entity->getEntityTypeId() == 'paragraph') {

      $parent_entity = $usage_entity->getParentEntity();
      $parent_field = $usage_entity->get('parent_field_name')->value;
      $parent_field_label = $parent_entity->get($parent_field)->getFieldDefinition()->getConfig($parent_entity->bundle())->getLabel();
      $field_label = "{$parent_field_label}:{$field_label}";
      $usage_entity = $parent_entity;
      $this->modifyEntity($usage_entity, $field_label);
    }
    // If the entity type is field collection, we need to find the parent entity
    // check if field collection module is enabled.
    if ($this->moduleHandler->moduleExists('field_collection') && $usage_entity->getEntityTypeId() == 'field_collection_item') {
      $parent_entity = $usage_entity->getHost();
      if ($parent_entity) {
        $parent_label = $parent_entity->getFieldDefinition($usage_entity->bundle())->label();
        $field_label = "{$parent_label} : {$field_label}";
        $usage_entity = $parent_entity;
        $this->modifyEntity($usage_entity, $field_label);
      }
      else {
        $usage_entity = FALSE;
      }
    }
  }

  /**
   * Returns a CSV file of usage data.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   * @param string $entity_type_id
   *   The entity type id.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   */
  public function usageCsv(Request $request, RouteMatchInterface $route_match, $entity_type_id = NULL) {
    $entity = $route_match->getParameter($entity_type_id);
    $usage_data = $this->getUsageData($entity);
    $page = $request->query->get('page') ?? 0;
    $rows = $this->getUsageRows($usage_data, $page);

    // Start using PHP's built in file handler functions
    // to create a temporary file.
    $handle = fopen('php://temp', 'w+');
    fputcsv($handle, [$this->t('Usage Report for @label', args: ['@label' => $entity->label() ?? $entity->id()])]);
    $header = array_keys($rows[array_key_first($rows)]);
    fputcsv($handle, $header);

    foreach ($rows as $row) {
      // For each value in reference, strip tags.
      $row = array_map('strip_tags', $row);
      fputcsv($handle, array_values($row));
    }
    // Reset where we are in the CSV.
    rewind($handle);

    // Retrieve the data from the file handler.
    $csv_data = stream_get_contents($handle);

    // Close the file handler since we don't need it anymore.  We are not storing
    // this file anywhere in the filesystem.
    fclose($handle);

    // This is the "magic" part of the code.  Once the data is built, we can
    // return it as a response.
    $response = new Response();

    // By setting these 2 header options, the browser will see the URL
    // used by this Controller to return a CSV file called "article-report.csv".
    $response->headers->set('Content-Type', 'text/csv');
    $filename = "usage-report-{$entity->getEntityTypeId()}-{$entity->id()}.csv";
    $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

    // This line physically adds the CSV data we created.
    $response->setContent($csv_data);

    return $response;
  }

}
