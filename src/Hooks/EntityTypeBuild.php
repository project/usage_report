<?php

namespace Drupal\usage_report\Hooks;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements hook_entity_type_build().
 *
 * @package Drupal\usage_report\Hooks
 */
class EntityTypeBuild implements ContainerInjectionInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new EntityTypeBuild instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * Helper function for hook_entity_type_build().
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface[] $entity_types
   *   The entity type.
   */
  public function build(array $entity_types) {
    foreach ($entity_types as $entity_type) {
      if ($entity_type instanceof ContentEntityTypeInterface && $entity_type->hasLinkTemplate('canonical')) {
        $entity_type->setLinkTemplate('usage-report', path: $entity_type->getLinkTemplate('canonical') . '/usage_report');
        $entity_type->setLinkTemplate('usage-report-csv', path: $entity_type->getLinkTemplate('canonical') . '/usage_report/csv');
      }
    }
  }

}
