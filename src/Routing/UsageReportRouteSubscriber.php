<?php

namespace Drupal\usage_report\Routing;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for entity Usage routes.
 */
class UsageReportRouteSubscriber extends RouteSubscriberBase {

  /**
   * Entity Type object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config Factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Creates a new RouteSubscriber instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ConfigFactoryInterface $config_factory) {
    $this->entityTypeManager = $entityTypeManager;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Get all content entity types.
    foreach ($this->entityTypeManager->getDefinitions() as $entity_type_id => $entity_type) {
      if ($entity_type instanceof ContentEntityTypeInterface && $entity_type->hasLinkTemplate('canonical')) {
        $route = new Route(
          $entity_type->getLinkTemplate('usage-report'),
          defaults: [
            '_controller' => '\Drupal\usage_report\Controller\UsageReportController::usage',
            'entity_type_id' => $entity_type_id,
          ],
          requirements: [
            '_permission' => 'view usage reports',
            '_entity_access' => $entity_type_id . '.update',
          ],
          options: [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            '_admin_route' => TRUE,
            'no_cache' => TRUE,
          ]
        );
        $route_name = "entity.$entity_type_id.usage_report";
        $collection->add($route_name, $route);

        $route = new Route(
          $entity_type->getLinkTemplate('usage-report-csv'),
          defaults: [
            '_controller' => '\Drupal\usage_report\Controller\UsageReportController::usageCsv',
            'entity_type_id' => $entity_type_id,
          ],
          requirements: [
            '_permission' => 'view usage reports',
            '_entity_access' => $entity_type_id . '.update',
          ],
          options: [
            'parameters' => [
              $entity_type_id => [
                'type' => 'entity:' . $entity_type_id,
              ],
            ],
            'no_cache' => TRUE,
          ]
        );
        $route_name = "entity.$entity_type_id.usage_report_csv";
        $collection->add($route_name, $route);
      }
    }
  }

}
